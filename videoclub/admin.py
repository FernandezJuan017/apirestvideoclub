from videoclub.models import Genero, Pelicula, Dvd, Cliente, Alquiler
from django.contrib import admin

admin.site.register(Genero)
admin.site.register(Pelicula)
admin.site.register(Dvd)
admin.site.register(Cliente)
admin.site.register(Alquiler)
