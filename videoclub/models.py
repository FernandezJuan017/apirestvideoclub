from django.db import models

#Generos
class Genero(models.Model):
    codigo = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=200)
    
    def __str__(self):
        return "(%s) %s" % (self.codigo, self.descripcion)
    
#Peliculas
class Pelicula(models.Model):
    titulo = models.CharField(max_length=50)
    director = models.CharField(max_length=100)
    genero =  models.ForeignKey(Genero, on_delete=models.CASCADE, related_name='peliculas')
    estreno = models.DateField()
    
    def __str__(self):
        return self.titulo
    
#Dvds
class Dvd(models.Model):
    identificador_dvd = models.CharField(max_length=50)
    pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE, related_name='dvds')
    disponible = models.BooleanField()
    
    def __str__(self):
        return "(%s) %s" % (self.identificador_dvd, self.pelicula.titulo)
    
#Clientes
class Cliente(models.Model):
    dni = models.CharField(max_length=20)          
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    direccion = models.CharField(max_length=255)
    numero_telefono = models.CharField(max_length=25)
    correo = models.CharField(max_length=100)
    
    def __str__(self):
        return f"({self.dni}) {self.apellido}, {self.nombre}"
    
#Alquileres
class Alquiler(models.Model):
    nro_recibo = models.CharField(max_length=150)
    #dni_cliente = models.CharField(max_length=15) #Me parece que queda mejor si la relacion con el cliente es por el id del cliente
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, related_name='alquileres') 
    fecha_alquiler = models.DateField() 
    fecha_devolucion = models.DateField(null=True,blank=True)
    precio = models.DecimalField(max_digits=10,decimal_places=2) 
    dvd = models.ForeignKey(Dvd, on_delete=models.CASCADE, related_name='alquileres')
    
    def __str__(self):
        return f"{self.nro_recibo}: {self.cliente.dni} - {self.dvd.identificador_dvd} ({self.dvd.pelicula.titulo})"