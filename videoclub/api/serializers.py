from videoclub.models import Cliente, Pelicula, Dvd, Genero, Alquiler
from rest_framework import serializers

#Genero
class GeneroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genero
        fields = "__all__"
        
#Cliente
class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = "__all__"
        
class UpdateClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = ['numero_telefono','correo']
         
#Pelicula
class PeliculaSerializer(serializers.ModelSerializer):
    #genero = serializers.StringRelatedField()
    genero = GeneroSerializer(read_only = True)
    class Meta:
        model = Pelicula
        fields = "__all__"
        
class CreatePeliculaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pelicula
        fields = "__all__"   
             
#Dvb
class DvdSerializer(serializers.ModelSerializer):
    pelicula = PeliculaSerializer(read_only = True)
    class Meta:
        model = Dvd
        fields = "__all__"

class CreateDvdSerializer(serializers.ModelSerializer):
    #pelicula = PeliculaSerializer(read_only = True)
    class Meta:
        model = Dvd
        fields = "__all__"
        
#Alquiler
class AlquilerSerializer(serializers.ModelSerializer):
    cliente = ClienteSerializer(read_only = True)
    dvd = DvdSerializer(read_only = True)
    fecha_alquiler = serializers.DateField(required=False, allow_null=True)
    class Meta:
        model = Alquiler
        fields = "__all__"
        
class UpdateAlquilerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alquiler
        fields = ['fecha_devolucion','precio']