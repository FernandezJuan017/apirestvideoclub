from django.urls import path
from videoclub.api.views import (ClienteCreate, ClienteUpdate, PeliculaDetail,
                                 PeliculaList, PeliculaCreate,DvdCreate, DvdList, GeneroDelete, AlquilerList,
                                 AlquilerUpdate)

urlpatterns = [
    path('dvds/', DvdList.as_view(), name = "dvds"),
    path('dvd/', DvdCreate.as_view(), name = "create_dvd"),
    path('genero/<int:pk>', GeneroDelete.as_view(), name="delete_genero"),
    path('cliente/', ClienteCreate.as_view(), name="create_cliente"),
    path('cliente/<int:pk>/', ClienteUpdate.as_view(), name='update_cliente'),
    path('pelicula/', PeliculaCreate.as_view(), name="create_pelicula"),
    path('peliculas/', PeliculaList.as_view(), name="peliculas"),
    path('pelicula/<int:pk>', PeliculaDetail.as_view(), name="delete_pelicula"),
    path('alquileres/', AlquilerList.as_view(), name = "alquileres"),
    path('alquiler/<int:pk>', AlquilerUpdate.as_view(), name = "update_alquiler")
]