from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, mixins
from videoclub.models import Cliente, Pelicula, Dvd, Genero, Alquiler
from videoclub.api.serializers import (ClienteSerializer,UpdateClienteSerializer ,
                                       PeliculaSerializer, CreatePeliculaSerializer, DvdSerializer, CreateDvdSerializer ,GeneroSerializer, AlquilerSerializer, UpdateAlquilerSerializer)

#Genero
class GeneroDelete(generics.DestroyAPIView):
    queryset = Genero
    serializer_class = GeneroSerializer
    
#Cliente
class ClienteCreate(generics.CreateAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    
class ClienteUpdate(mixins.UpdateModelMixin, generics.GenericAPIView):
    queryset = Cliente.objects.all()
    serializer_class = UpdateClienteSerializer
    
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
    
#Pelicula
class PeliculaDetail(generics.DestroyAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializer
    
class PeliculaCreate(generics.CreateAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = CreatePeliculaSerializer
 
class PeliculaList(generics.ListAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['genero']
      
 #Dvd
class DvdCreate(generics.CreateAPIView):
    queryset = Dvd.objects.all()
    serializer_class = CreateDvdSerializer
    
class DvdList(generics.ListAPIView):
    queryset = Dvd.objects.all()
    serializer_class = DvdSerializer

#Alquiler
class AlquilerList(generics.ListAPIView):
    serializer_class = AlquilerSerializer
    
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['fecha_alquiler']

    def get_queryset(self):
        queryset = Alquiler.objects.all()
        
        dni = self.request.query_params.get('dni')
        fecha_alquiler = self.request.query_params.get('fecha_alquiler')
        
        if dni is not None:
            queryset = queryset.filter(cliente__dni=dni)
            
        #Queda mejor si uso filter_backends para la fecha_alquiler
        # if fecha_alquiler is not None:
        #     queryset = queryset.filter(fecha_alquiler=fecha_alquiler)
            
        return queryset
    
class AlquilerUpdate(mixins.UpdateModelMixin, generics.GenericAPIView):
    queryset = Alquiler.objects.all()
    serializer_class = UpdateAlquilerSerializer
    
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)